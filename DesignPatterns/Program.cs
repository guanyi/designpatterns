﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.StructualPatterns;
using DesignPatterns.StructualPatterns.Adapter;
using DesignPatterns.StructualPatterns.Decorator;
using DesignPatterns.StructualPatterns.Proxy;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //ClassAdapterTest a = new ClassAdapterTest();

            //ObjectAdapterTest a = new ObjectAdapterTest();

            //InterfaceAdapterTest a = new InterfaceAdapterTest();

            //DecoratorTest a = new DecoratorTest();
            ProxyTest a = new ProxyTest();



            a.ShowTest();
            Console.ReadLine();
        }
    }
}
