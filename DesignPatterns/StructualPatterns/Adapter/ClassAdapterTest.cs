﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StructualPatterns
{
    internal class ClassAdapterTest
    {
        /*Studnet class and Planet class are two different unrelated classes, but what if I want an instance of a Student
          use the ShowInfo() method in Planet class? The key idea of class adapter is to extend a class's (Planet) method
          to another class (Student).

        For example how to make
        Student student = <somne code>, then
        student.ShowInfo(); statement can work with no error?

        We cannot use
        Student student = new Student();
        student.ShowInfo();      because this student is not a type of Planet

        We also cannot use
        Student student = new Palnet();
        student.ShowInfo();      because we cannot new a Planet instance then assign it to a Student variable

        What should we do?
        Answer: we should create a hybrid class and this class can use both methods from Student ans Planet class.
                This hubrid class is our class Adapter
         */

        public void ShowTest()
        {
            IStudent student = new PlanetAdapter();
            student.DoSomething();
            student.ShowInfo();
        }
    }
    internal class Student
    {
        public void DoSomething()
        {
            Console.WriteLine("This menthod is done by an instance of a Student class");
        }
    }

    internal class Planet
    {
        public void ShowInfo()
        {
            Console.WriteLine("This is the information about a planet.");
        }
    }

    /*----------------------------------------------------*/
    //Below this line is the solution to deal with this situation

    internal interface IStudent
    {
        void DoSomething();
        void ShowInfo();
    }

    internal class PlanetAdapter : Planet, IStudent
    {
        public void DoSomething()
        {
            Console.WriteLine("This menthod is done by an implemented instance of a Student class");
        }

        //this ShowInfo() method should not be overriden, as we just need to use the method from Planet class,
        //even though we are able to override
        //public override void ShowInfo()
        //{
        //    Console.WriteLine("This is the information about a planet from adapter.");
        //}
    }


}
