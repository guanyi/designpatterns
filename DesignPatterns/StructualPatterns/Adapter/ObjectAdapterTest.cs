﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StructualPatterns.Adapter
{
    class ObjectAdapterTest
    {
        public void ShowTest()
        {
            /* Object adapter is slightly different from Class adapter. Object adapter need to call
             * a method in class Student, but it actually perform another function which is in class
             * Planet. Student and Planet may not have any relation.
             * 
             * It is like a converter from one method to another class's method. In a situation like
             * we get a new version of method parameter interface. Before the old interface to draw
             * a rectengle is to pass 4 points x y position, like 
             * public void drawRec(Point one, Point two, Point three, Point four) {}
             * 
             * but the new class's api lets us pass 2 points x y position and 2 side length like
             * public void drawRec(Point one, Point two, int width, int height) {}
             * 
             * We need a way to convert such as
             * 
             * newWayInstance.draw(one, two, 12, 20);  //will actually call the old way to draw
             * 
             * public void drawRec(Point one, Point two, int width, int height)
             * {
             *     //do some manual convertion to convery width and height to
             *     //Point three and Point four, then call
             *     
             *     oldWayInstance.drawRec(one, two, three, four);
             * }
             * 
             * The key point is to keep a oldWatInstance variable in the adapter which convert the
             * new api to old api. The adapter is a convert or like a wrapper.
             */
            IStudent student = new PlanetAdapter(new Planet());
            student.DoSomething();
            //student.ShowInfo();
        }
    }
    public class Student
    {
        public void DoSomething()
        {
            Console.WriteLine("This menthod is done by an instance of a Student class");
        }
    }

    public class Planet
    {
        public virtual void ShowInfo()
        {
            Console.WriteLine("This is the information about a planet.");
        }
    }

    /*----------------------------------------------------*/
    //Below this line is the solution to deal with this situation

    internal interface IStudent
    {
        void DoSomething();
    }

    internal class PlanetAdapter : IStudent
    {
        Planet planet;
        public PlanetAdapter(Planet planet) : base()
        {
            this.planet = planet;
        }

        public void DoSomething()
        {
            //some convetion needs to be done here in real situation
            planet.ShowInfo();
        }
    }
}

