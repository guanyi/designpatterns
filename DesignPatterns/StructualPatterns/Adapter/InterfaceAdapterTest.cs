﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StructualPatterns.Adapter
{
    class InterfaceAdapterTest
    {
        /*When we need to use a big interface which has a lot of methods need to be implemented, but
         we only need implement part of them. What if we have many classes need to implement this
         big interface, each class need to implement all these methods? That a waste
         
        Solution is to create a abstract class which implements all methods in the big interface, then
        all the concreate classes only override what we need to override.
         */

        public void ShowTest()
        {
            IBigInterface obj = new ConcreateClass();
            obj.method1();
        }

        internal interface IBigInterface
        {
            void method1();
            void method2();
            void method3();
        }

        abstract class MiddleAdapter: IBigInterface
        {
            //these methods can be written as 
            //public virtual void method1() {} if there is no actual need

            //c# must use virtual keyword to indicate this method can be overriden in run time
            public virtual void method1()
            {
                Console.WriteLine("I am method one in MiddleAdapter");
            }
            public void method2()
            {
                Console.WriteLine("I am method two in MiddleAdapter");
            }
            public void method3()
            {
                Console.WriteLine("I am method three in MiddleAdapter");
            }
        }

        //It shows that we only need to implement one method, not all method in IBigInterface
        class ConcreateClass : MiddleAdapter
        {
            //c# need override keyword to actually override method1() in MiddleAdapter
            public override void method1()
            {
                Console.WriteLine("I am method one in concreate class");
            }
        }

    }
}
