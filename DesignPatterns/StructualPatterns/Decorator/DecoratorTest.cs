﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StructualPatterns.Decorator
{
    class DecoratorTest
    {
        public void ShowTest()
        {
            //The power of decorator is that it can chain the creation of decorated instance.
            //Programmers can dynamically create a TextWriter by using any combinations of decorated features.
            //Java buffer stream outout classes and android framework Connext and it subclasses use this feature
            TextWriter textWriter = new TextWriterBackgroundColor(new TextWriterForegroundColor(new ConcreateTextWriter()));
            textWriter.WriteText("Message");
        }
    }


    //The interface is just a way to connect ConcreateTextWriter and Decorator abstract class, then all sub classes under
    //Decorators will also be connected
    interface TextWriter
    {
        void WriteText(string text);
    }

    //This is the class with the very basic function. We may need to dynamically add some features based on its basic function
    class ConcreateTextWriter: TextWriter
    {
        public void WriteText(string text)
        {
            Console.WriteLine(text);
        }
    }

    //The abstrct decorator class is the head of all concreate decorator classes.
    //This abstrct decorator class holds an reference of TextWriter, this is why an instance of class which implements
    //TextWriter interface can be chained in the instantiation of object
    abstract class TextWriterDecorator : TextWriter
    {
        TextWriter textWriter;
        public TextWriterDecorator(TextWriter textWriter)
        {
            this.textWriter = textWriter;
        }
        public virtual void WriteText(string text)
        {
            textWriter.WriteText(text);
        }
    }

    //Each of the following 2 concrete decorator classes only adds one feature to the basci TextWriter, but they can be
    //recursively chained. Each class only pass the TextWriter reference to constructor
    //The abstract TextWriterDecorator class helps all the concrete decorator classes to avoid repeat the same code.
    //If we do not worry about repeat code, then the abstrct class is not necesary? May need to test again
    class TextWriterForegroundColor : TextWriterDecorator
    {
        public TextWriterForegroundColor(TextWriter textWriter) : base(textWriter) { }
        public override void WriteText(string text)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            base.WriteText(text);
        }
    }

    class TextWriterBackgroundColor : TextWriterDecorator
    {
        public TextWriterBackgroundColor(TextWriter textWriter) : base(textWriter) { }
        public override void WriteText(string text)
        {
            Console.BackgroundColor = ConsoleColor.DarkGray;
            base.WriteText(text);
        }
    }

}
