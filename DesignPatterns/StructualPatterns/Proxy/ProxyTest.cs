﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StructualPatterns.Proxy
{
    /* One chractiristic of proxy is that the proxy has the same interface as the real object
     * Adapter is to covert an interface to another interface*/
    internal class ProxyTest
    {
        private IWebSocket _connector;
        public void ShowTest()
        {
            _connector = new WebSocketProxy();
            Console.Write(_connector.GetMessage());
        }
    }

    /*The interface is depend upon the ReadWebSocket. I think we know the RealWebSocket has a GetMessage() method, then
    we create this interface with GetMessage() method. The idea of proxy is to create a wrapper which has the same
    interface as the real object.

    Remote proxies: They are responsible for representing the object located remotely. Talking to the real object
                    might involve marshalling and unmarshalling of data and talking to the remote object.
                    All that logic is encapsulated in these proxies and the client application need not worry about them.
    Virtual proxies: These proxies will provide some default and instant results if the real object is supposed to take
                     some time to produce results. These proxies initiate the operation on real objects and provide a default
                     result to the application. Once the real object is done, these proxies push the actual data to the client
                     where it has provided dummy data earlier.
    Protection proxies: If an application does not have access to some resource then such proxies will talk to the objects
                        in applications that have access to that resource and then get the result back.
    */
    internal interface IWebSocket
    {
        string GetMessage();
    }

    internal class RealWebSocket: IWebSocket
    {
        public string GetMessage()
        {
            return "This is the information get from real websocket";
        }
    }

    //The questions is that why we wrap the real websocket to do the same thing.
    //1, if the creation and setup if complcated
    //2, 
    internal class WebSocketProxy: IWebSocket
    {
        private readonly RealWebSocket _realWebSocket;
        public WebSocketProxy()
        {
            if (_realWebSocket == null)
            {
                //there might be some complicated setup for this real websocket,
                //but the proxy object will hide it here, the real implementation
                _realWebSocket = new RealWebSocket();
            }
        }

        public string GetMessage()
        {
            return _realWebSocket.GetMessage();
        }
    }
}
